<?php
// TODO: Fix it so it requests the rest of the playlist items (beyond first 30)
$url = "https://open.spotify.com/playlist/37i9dQZF1DX2MyUCsl25eb?si=uBEtUccITOug6Qw4UAgX6A";
if (false === $rawData = RetrieveRawPlaylist($url))
    exit();
$playlist = ParseOutData($rawData);

// output
$delimeter = ",";
if (isset($_REQUEST["delimeter"]) && $_REQUEST["delimeter"] == "tab")
    $delimeter = "\t";

$line_end = "\r\n";
if (isset($_REQUEST["line_end"])) {
    if ($_REQUEST["line_end"] == "cr")
        $line_end = "\r";
    if ($_REQUEST["line_end"] == "lf")
        $line_end = "\n";
}

if (count($playlist) > 0) {
    header('Content-type: text/csv');
    header('Content-disposition: attachment;filename=playlist.csv');
    
    echo implode($delimeter, array_keys($playlist[0])) . $line_end;
    foreach ($playlist as $entry) {
        echo Quotify($delimeter, $entry["title"]) . $delimeter;
        echo Quotify($delimeter, $entry["artist"]) . $delimeter;
        echo Quotify($delimeter, $entry["album"]) . $delimeter;
        echo Quotify($delimeter, implode(", ", $entry["features"])) . $line_end;
    }
} else {
    echo 'An error occurred. No songs found.';
}

function Quotify($delimeter, $inStr) {
    if (false === strpos($inStr, $delimeter))
        return $inStr;
    else
        return "\"" . $inStr . "\"";
}

function RetrieveRawPlaylist($url) {
    $options = array(
        CURLOPT_URL => $url,
        CURLOPT_HEADER => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 15,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_USERAGENT => "spider",
        CURLOPT_AUTOREFERER => true,
        CURLOPT_SSL_VERIFYPEER => false
    );

    $ch = curl_init();
    curl_setopt_array($ch, $options);
    if (false === $output = curl_exec($ch)) {
        echo 'An error happened. sorry.';
        var_dump(curl_getinfo($ch));
    }
    curl_close($ch);

    return $output;
}

function ParseOutData($playlistHtml) {
    $playlist = array();
    
    // Tidy the XML, this method is extremely fragile; needs to be fixed
    $start = strpos($playlistHtml, "<ol class=\"tracklist");
    $stop = strpos($playlistHtml, "</ol>", $start);
    
    $htmlList = html_entity_decode(substr($playlistHtml, $start, $stop - $start + 5));
    
    //echo htmlentities($htmlList);exit();
    
    $doc = new DOMDocument();
    if (false === $doc->loadXml($htmlList)) {
        echo 'Failure.';
        return $playlist;
    }
    $domXPath = new DOMXPath($doc);
    
    $tracks = $domXPath->query("//div[@class='top-align track-name-wrapper']");
    foreach ($tracks as $t1) {
        $track = new DOMDocument();
        $track->loadXml($t1->ownerDocument->saveXML($t1));
        $trackXPath = new DOMXPath($track);
        
        $entry = array(
            "title" => urldecode($trackXPath->query("//span[@class='track-name']")->item(0)->nodeValue),
            "artist" => "",
            "album" => "",
            "features" => array()
        );
        
        for ($i = 0; $i < $trackXPath->query("//span[@class='artists-albums']/a")->length; $i++) {
            if ($i == 0)
                $entry["artist"] = $trackXPath->query("//span[@class='artists-albums']/a/span")->item($i)->nodeValue;
            else if ($i == $trackXPath->query("//span[@class='artists-albums']/a")->length - 1)
                $entry["album"] = $trackXPath->query("//span[@class='artists-albums']/a/span")->item($i)->nodeValue;
            else {
                $entry["features"][] = $trackXPath->query("//span[@class='artists-albums']/a/span")->item($i)->nodeValue;
            }
        }
        
        $playlist[] = $entry;
    }
    
    return $playlist;
}